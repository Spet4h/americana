require('dotenv').config()
import  Usuario from '../models/usuario'
import  bcrypt  from 'bcrypt'
import  token from '../services/token'

exports.login = async(req,res,next) =>{
    try{
        let user = await Usuario.findOne({correo:req.body.correo});
        if(user){
          let match = await bcrypt.compare(req.body.password, user.password);
          if(match){
            let tokenReturn = await token.encode(user._id,user.role,user.correo,user.nombre);
            res.status(200).json({user,tokenReturn})
          }else{ 
            res.status(404).send({
              message: 'Password Incorrecto'
            })
          }
        }else{
          res.status(404).send({
            message: 'No existe el usuario'
          });
        }   
    }catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)
    }
}