import express from 'express'
import encargadoController from '../controllers/encargadoController'
import auth from '../middlewares/auth';

const router = express.Router()

router.post('/encargado',auth.verifyAdministrador,encargadoController.addEncargado)
router.get('/encargados',auth.verifyAdministrador,encargadoController.getEncargados)
router.get('/encargado/:id',auth.verifyAdministrador,encargadoController.getEncargado)
router.put('/encargado/:id',auth.verifyAdministrador,encargadoController.updateEncargado)
router.delete('/encargado/:id',auth.verifyAdministrador,encargadoController.deleteEncargado)

export default router;