import express from 'express'
import areaController from '../controllers/areaController'
import auth from '../middlewares/auth';


const router = express.Router()

router.post('/area',auth.verifyAdministrador,areaController.addArea)
router.get('/areas',auth.verifyAdministrador,areaController.getAreas)
router.get('/area/:id',auth.verifyAdministrador,areaController.getArea)
router.put('/area/:id',auth.verifyAdministrador,areaController.updateArea)
router.delete('/area/:id',auth.verifyAdministrador,areaController.deleteArea)

export default router 