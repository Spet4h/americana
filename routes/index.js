import express from 'express'
import equipoRouter from './equipo'
import usuarioRouter from './usuario'
import encargadoRouter from './encargado'
import areaRouter from './area'
import reporteRouter from './reporte'
import empresaRouter from './empresa'
import loanRouter from './loan'
import uploadEmpresa from './uploadEmpresa'
import uploadEquipo from './uploadEquipo' 
import uploadUser from './uploadUser'

const router = express.Router()

router.use(equipoRouter)
router.use(usuarioRouter)
router.use(encargadoRouter)
router.use(areaRouter)
router.use(reporteRouter)
router.use(empresaRouter)
router.use(loanRouter)
router.use(uploadEmpresa)
router.use(uploadEquipo)
router.use(uploadUser)

export default router 