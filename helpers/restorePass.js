import Usuario      from '../models/usuario'
import randomString from 'random-string'
import nodemailer   from 'nodemailer'
import  bcrypt      from 'bcrypt'
  /*=================== 
    Method POST - Send email to restore password 
  ===================*/

  const getRestorePasswordUrl =  async(req,res,next)=>{
    try {
     const user = await Usuario.findOne({correo:req.body.correo});
     if(!user){
       return res.status(400).send("No existe un usuario con este email");
     }  

     let tokenR  = randomString({
      length: 8,
      numeric: true,
      letters: false,
    });
     let rutaLocal = process.env.BASE_URL;
     // let link = `${rutaLocal}/password-reset/${user._id}/${token.token}` ORIGINAL
     let link = `${rutaLocal}/password-reset/${user._id}/${tokenR}`
     
    //  Sending email
    let transporter = nodemailer.createTransport({
      host:process.env.BASE_URL,
      port:587,
      secure:true,
      service: process.env.SERVICE,
      auth:{
        user: process.env.EMAIL_FROM,
        pass: process.env.PASS 
      }
    });
    let mailOptions = {
      // from: process.env.EMAIL_FROM,
      from: `"Cua" <${process.env.EMAIL_FROM}>`,
      to: user.correo,
      subject: 'Sistema Gestor de equipos Américana',
      text: `Restablecer contraseña, sistema gestor de equipos Corporación Universitaría Américana - Sede Montería. Recuerda que para que tu contraseña sea más segura debes utilizar al menor 8 caracteres incluir minusculas, mayusculas, números y caracteres especiales 
       Password reset, ${link}
       `,
    };
    await transporter.sendMail(mailOptions,(error,info)=>{
      if (error) {
        console.log(error);
      } else {
        console.log('Email enviado: ' + info.response);
        // info client
        res.status(200).send('Email send successfully...')
      }
    });

    }catch(err){
      next(err)
    }
  }
  // method POST
 const restorePassword = async(req,res,next)=>{
   try{
     // CODE WEB
     const user = await Usuario.findById(req.params.userId);
       if (!user) {
         return res.status(400).send("invalid link or expired / o no existe el usuario")
       };
       user.password = req.body.password;
       // Pass Encrip
       user.password = await bcrypt.hashSync(user.password,10);
       // Pass Encrip
       await user.save();
       // console.log(user.password);
       // await tok.delete();
       res.json({
         message:'Password reset succcesfully'
       })
      //  res.send("password reset sucessfully.");

  }catch (error) {
   next(error)
   }
 }
 
const verifyPasswordRestore = async (req, res, next) => {
   try {
       const { userId, token } = req.params
       if (!userId || !token || token == '')
           return res.status(400).json({success: false, info: 'Invalid data structure'})
       const data = await Usuario.findOne({_id: userId})
       if (!data || !data._id)
           return res.status(400).json({success: false, info: 'Invalid token'})
       res.status(200).json({success: true, info: 'Correct info',data})
       console.log(data);
   } catch (error) {
       next(error)
   }
}

module.exports = {
  getRestorePasswordUrl,
  restorePassword,
  verifyPasswordRestore  
}