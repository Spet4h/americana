import mongoose, { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator'
const EncargadoSchema = new Schema({
   nombre: {type: String,required:[true,'El nombre es obligatorio']},
   apellido: {type:String},
   celular: {type: Number, maxLength:10, unique:true,required:[true,'el telefono es obligatorio']},
   cedula: {type:Number,unique:true,unique:true, required:[true,'La cédula es obligatoria']},
   state:{type:Boolean,default:true},
   createAt:{type:Date,default:Date.now()}
  },{versionKey:false});

EncargadoSchema.plugin(uniqueValidator,{message: 'Error el {PATH} debe ser único'})
const Encargado = mongoose.model('Encargado',EncargadoSchema);
export default Encargado;



/* Nota:
  El encargado dirige una area (existen varias...  )
*/
